import React, { useState } from 'react';
import Summary from './components/Summary';
import List from './components/List';
import Control from './components/Control';

const App = () => {
  // const [block, setBlock] = useState({});
  const [value, setValue] = useState(0);
  const [blocksCount, setBlocksCount] = useState(0);
  const [blocksList, setBlocksList] = useState([]);

  const addBlock = (e) => {
    setValue((prevState) => {
      return prevState + 5;
    });

    setBlocksCount((prevState) => {
      return prevState + 1;
    });

    const newBlock = {
      id: new Date().getTime().toString(),
      type: 'block',
      cost: 5,
    };
    setBlocksList([...blocksList, newBlock]);
  };

  const removeBlock = (id) => {
    let updatedList = blocksList.filter((block) => block.id !== id);
    setBlocksList(updatedList);

    setValue((prevState) => {
      return prevState - 5;
    });

    setBlocksCount((prevState) => {
      return prevState - 1;
    });
  };

  const resetList = () => {
    setValue(0);
    setBlocksCount(0);
    setBlocksList([]);
  };

  return (
    <div className='container'>
      <Summary value={value} blocksCount={blocksCount} />
      <List blocksList={blocksList} removeBlock={removeBlock} />
      <Control
        blocksList={blocksList}
        addBlock={addBlock}
        resetList={resetList}
      />
    </div>
  );
};

export default App;
