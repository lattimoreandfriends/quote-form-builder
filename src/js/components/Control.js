import React from 'react';

const Control = ({addBlock, resetList, blocksList}) => {
  
  return (
    <div className='control'>
      <button className='btn btn--add' type='button' onClick={addBlock}>
        add a block
      </button>

      {blocksList.length > 0 && (
        <button className='btn btn--reset' type='button' onClick={resetList}>
          start again
        </button>
      )}
    </div>
  );
};

export default Control;
