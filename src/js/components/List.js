import React from 'react';
import Block from './Block';

const List = ({ blocksList, removeBlock }) => {

  return (
    <div className='list'>
      {blocksList.map((block) => {
        const { id, type, cost } = block;
        return (
          <Block key={id} id={id} type={type} cost={cost} removeBlock={removeBlock} />
        );
      })}
    </div>
  );
  
};

export default List;
