import React from 'react';
const Block = ({id, type, cost, removeBlock }) => {
  return (
    <div className='block'>
      <p>block type: {type}</p>
      <p>block cost: {cost}</p>
      <button
        className='btn btn--remove'
        type='button'
        onClick={() => removeBlock(id)}
      >
        remove block
      </button>
    </div>
  );
};

export default Block;
