import React from 'react';
const Summary = ({ value, blocksCount }) => {
  return (
    <div className='summary'>
      <h2>how much will it cost?</h2>
      <p>this much: {value}</p>
      <p>blocks: {blocksCount}</p>
    </div>
  );
};

export default Summary;
